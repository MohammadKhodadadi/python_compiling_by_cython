# coding: utf-8

import os
from setuptools import setup, find_packages
from Cython.Build import cythonize

CURRENT_DIR = os.getcwd()

EXCLUDE_FILES = [
    os.path.join('app', 'main.py'),
    os.path.join('app', '__init__.py'),

]


def get_ext_paths(root_dir, exclude_files):
    """get filepaths for compilation"""
    paths = []

    for root, dirs, files in os.walk(root_dir):
        for filename in files:
            if os.path.splitext(filename)[1] != '.py':
                continue

            file_path = os.path.join(root, filename)
            if file_path in exclude_files:
                continue

            paths.append(file_path)
    return paths


print("--------------------------------")
print(get_ext_paths(CURRENT_DIR, EXCLUDE_FILES))
print("--------------------------------")
setup(
    name='app',
    version='0.1.0',
    packages=find_packages(),
    ext_modules=cythonize(
        get_ext_paths(CURRENT_DIR, EXCLUDE_FILES),
        compiler_directives={'language_level': 3}
    )
)
